# openquiz-export

Программа позволяет экспортировать результаты с [Опенквиза](https://open-quiz.com) в формат [турнирного сайта](https://rating.chgk.info).

## Установка

`pip install openquiz_export`

## Использование

### Интерфейс

1\. Внесите через веб-интерфейс турнирного сайта участвовавшие команды (можно без составов, просто чтобы были id, название и город).  
2\. Запустите в терминале `oq-export` и введите данные, как указано на скриншоте:

![](pic.png)

### Командная строка

Если составы уже внесены на турнирный сайт: `oq-export -t ID_ТУРНИРА -u CCЫЛКА_НА_РЕЗУЛЬТАТЫ -v "Название Площадки"`